#!/bin/bash

# Script that runs on laptop startup
# This script is called by the crontab file
# This script checks if tmux is installed in the system
# If tmux is installed, it runs a session called "hubos" with 4 windows
# The first window is called "backend" and the source directory is ~/workspace/src/gitlab.com/hub-buildings/services/hub-os-backend-v1
# The second window is called "infra" and the source directory is ~/workspace/src/gitlab.com/hub-buildings/platform/infrastructure
# The third window is called "env" and the source directory is ~/workspace/pipelines/digitalocean/hub-buildings/
# The fourth window is called "home" and the source directory is ~/
# If tmux is not installed, it echoes a message to the terminal and exits

# Strict mode
set -euo pipefail
IFS=$'\n\t'

if [ -x "$(command -v tmux)" ]; then
    tmux new-session -d -s hubos -n backend -c /home/morel/workspace/src/gitlab.com/hub-buildings/services/hub-os-backend-v1
    tmux new-window -t hubos:1 -n infra -c /home/morel/workspace/src/gitlab.com/hub-buildings/platform/infrastructure
    tmux new-window -t hubos:2 -n migrations  -c /home/morel/workspace/src/gitlab.com/hub-buildings/hub-migrations
    tmux new-window -t hubos:3 -n env -c /home/morel/workspace/pipelines/digitalocean/hub-buildings/
    tmux new-window -t hubos:4 -n graylog -c /home/morel/workspace/src/gitlab.com/hub-buildings/platform/graylog-documentor
    tmux new-window -t hubos:5 -n home -c /home/morel
    tmux select-window -t hubos:0
    tmux -2 attach-session -t hubos
else
    echo "tmux is not installed"
fi
